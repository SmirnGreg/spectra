import warnings

import matplotlib.pyplot as plt
import numpy as np

R = 8.31


class Planet:
    def __init__(self, t_eff=255., mu=0.028, cp=None, gamma=1.4):
        if mu >= 1:
            warnings.warn("Value of mu ({}) kg mole-1 is too big! Make sure that you send mu in kg mole-1".format(mu))
        self.t_eff = t_eff
        self.mu = mu  # g mole-1
        if cp is None:
            self.cp = self.calculate_cp(self.mu)
        else:
            self.cp = cp
        self.t_skin = 2 ** (-0.25) * self.t_eff
        self.gamma = gamma  # it is big Gamma, Gamma = 1-1/gamma

    def calculate_cp(self, mu, gamma=1.4):  # TODO class Abundances(dict)
        i = 2 / (gamma - 1)
        cp = (i + 2) / 2. * R / mu
        return cp

    def p_t_profile(self, p, p0=0.1):

        def get_p_t_profile(p_loc):
            if p_loc < p0:
                return self.t_skin
            else:
                # print(R/self.cp)
                return self.t_skin * (p_loc / p0) ** (self.gamma)

        try:
            p_iterator = iter(p)
            return np.array(list(map(get_p_t_profile, p)))
        except TypeError:
            return get_p_t_profile(p)


if __name__ == '__main__':
    earth = Planet(t_eff=255, mu=0.028)
    p = np.linspace(0, 1)
    temp = earth.p_t_profile(p)
    print('T_skin: ', earth.t_skin)
    plt.gca().invert_yaxis()
    plt.semilogy(temp, p)
    plt.show()
