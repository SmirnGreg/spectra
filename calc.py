#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import warnings
from pprint import pprint

import astropy.io.ascii as io
import numpy as np
import plotly.graph_objs as go
import plotly.plotly as py
from planet import Planet
from plotly.offline import iplot as off_iplot
from public_transmission import nat_cst as nc
from public_transmission import radtrans as rt
from scipy.ndimage.filters import *

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-pi", "--planets_in", help="input table of planets (planets.in)", type=str,
                        default="planets.in")

    parser.add_argument("-ai", "--abundances_in", help="input table of mole abundances (abundances.in)", type=str,
                        default="abundances.in")

    parser.add_argument("-o", "--outputfile", help="output filename template (out)", type=str, default="out")

    parser.add_argument("-v", '--verbose',
                        help="increase verbosity [-v, -vv]", action='count', default=0)

    parser.add_argument("-e", '--emission',
                        help="calculate emission instead of transmission (False)", action='store_true', default=False)

    parser.add_argument("--p_surf",
                        help="surface pressure, bars (1)", type=float, default=1)

    parser.add_argument("--p_trop",
                        help="troposphere limit pressure, bars (0.1)", type=float, default=0.1)

    parser.add_argument("--new_setup",
                        help="the planet id (starting with 0) for new setup (None)", type=int, default=None)

    parser.add_argument("--new_d_gamma",
                        help="value to add to adiabatic gamma for new setup (0)", type=float, default=0)

    parser.add_argument("--new_p_surf",
                        help="surface pressure for new setup, bars (p_surf)", type=float, default=None)

    parser.add_argument("--new_p_trop",
                        help="troposphere limit pressure for new setup, bars (p_trop)", type=float, default=None)

    parser.add_argument("-d", "--plot_difference",
                        help="plot difference between new and old setups instead of new setup plots (False)",
                        action='store_true', default=False)
    args = parser.parse_args()
    p_surf = args.p_surf
    p_trop = args.p_trop
    new_setup = args.new_setup

    if args.new_p_surf is None:
        args.new_p_surf = p_surf
    new_p_surf = args.new_p_surf
    if args.new_p_trop is None:
        args.new_p_trop = p_trop
    new_p_trop = args.new_p_trop

    new_d_gamma = args.new_d_gamma
    verbose = args.verbose

    if verbose:
        print('Input parameters:')
        for key in vars(args):
            print("{} = {}".format(key, getattr(args, key)))


    press = np.logspace(-9, np.log10(p_surf), 90)

    ### Define molecular weights
    mu = dict(
        CO2=44,
        O2=32,
        H2O=18,
        N2=28,
        O3=48,
        CH4=16,
        NH3=17
    )


    def mmw_calc(mu: dict, abunds: dict):
        mu_abu = []
        for species in abunds.keys():
            mu_abu.append(mu[species] * abunds[species])
        return np.sum(mu_abu, axis=0)


    def volume2mass_abundances(mu: dict, abunds: dict):
        mmw = mmw_calc(mu, abunds)
        abu_mass = {}
        for species in abunds.keys():
            abu_mass[species] = abunds[species] * mu[species] / mmw
        return abu_mass


    # Load abundances.in
    setups_list = []
    try:
        setups_table = io.read(args.abundances_in, comment="\s*#")
        if verbose:
            setups_table.pprint()
        for setup_in in setups_table:
            abundances = dict([
                (abu_col, setup_in[abu_col] * np.ones_like(press))
                for abu_col in setup_in.colnames[3:]
            ])
            mmw = mmw_calc(mu, abundances)
            abundances_mass = volume2mass_abundances(mu, abundances)
            setup = dict(
                name=setup_in['name'],
                color=setup_in['color'],
                gamma=setup_in['Gamma'],
                abundances=abundances_mass,
                mmw=mmw
            )
            setups_list.append(setup)
        print('Setups are loaded from {}'.format(args.abundances_in))
    except Exception as e:
        print(e)
        warnings.warn('Abundances were not loaded from {}, default setup is set!'.format(args.abundances_in))
        abunds = {}
        abunds['CO2'] = np.ones_like(press) * 0.000387
        abunds['O2'] = np.ones_like(press) * 0.20946
        abunds['H2O'] = np.ones_like(press) * 0.003
        abunds['N2'] = np.ones_like(press) * 0.78084
        abunds['O3'] = np.ones_like(press) * 1e-8
        abunds['CH4'] = np.ones_like(press) * 0
        abunds['NH3'] = np.ones_like(press) * 0
        mmw_earth = mmw_calc(mu, abunds)
        abunds_earth = volume2mass_abundances(mu, abunds)
        setups_list.append(dict(
            name='earth',
            mmw=mmw_earth,
            abundances=abunds_earth,
            color='green',
            gamma=0.29
        ))

        abunds = {}
        abunds['CO2'] = np.ones_like(press) * 0.965
        abunds['O2'] = np.ones_like(press) * 0.0
        abunds['H2O'] = np.ones_like(press) * 2e-5
        abunds['N2'] = np.ones_like(press) * 0.034
        abunds['O3'] = np.ones_like(press) * 0
        abunds['CH4'] = np.ones_like(press) * 0
        abunds['NH3'] = np.ones_like(press) * 0
        mmw_venus = mmw_calc(mu, abunds)
        abunds_venus = volume2mass_abundances(mu, abunds)
        setups_list.append(dict(
            name='venus',
            mmw=mmw_venus,
            abundances=abunds_venus,
            color='blue',
            gamma=0.22
        ))

        abunds = {}
        abunds['CO2'] = np.ones_like(press) * 0.00000000001
        abunds['O2'] = np.ones_like(press) * 0.00000000001
        abunds['H2O'] = np.ones_like(press) * 0.5e-4
        abunds['N2'] = np.ones_like(press) * 0.984
        abunds['O3'] = np.ones_like(press) * 0.
        abunds['CH4'] = np.ones_like(press) * 0.014
        abunds['NH3'] = np.ones_like(press) * 1e-4
        mmw_titan = mmw_calc(mu, abunds)
        abunds_titan = volume2mass_abundances(mu, abunds)

        setups_list.append(dict(
            name='titan',
            mmw=mmw_titan,
            abundances=abunds_titan,
            color='black',
            gamma=0.29
        ))
    if verbose > 1:
        pprint(setups_list)

    # Load planets.in
    names = []
    temp_list = []
    radius_list = []
    gravity_list = []
    try:
        planets_table = io.read(args.planets_in)
        if verbose: planets_table.pprint()
        for planet in planets_table:
            names.append(planet['Planet'])
            temp_list.append(planet['t_eff'])
            radius_list.append(planet['radius'])
            gravity_list.append(planet['gravity'])
        # make them constant:
        names = tuple(names)
        temp_list = tuple(temp_list)
        radius_list = tuple(radius_list)
        gravity_list = tuple(gravity_list)
        print('Planets are loaded from {}'.format(args.abundances_in))
    except:
        warnings.warn('Planets were NOT loaded from {}, default setup is set!'.format(args.planets_in))
        names = ('GJ 1132b', 'TRAPPIST-1c', 'LHS 1140b', 'GJ 1132b-t', 'TRAPPIST-1c-t', 'LHS 1140b-t',)
        temp_list = (530., 320., 200., 530., 320., 200.,)
        radius_list = (1.16, 1.07, 1.43, 1.16, 1.07, 1.43,)
        gravity_list = (1130., 1420., 3350., 1130., 1420., 3350.)

    ### Create RT object

    line_species = ['CO2', 'H2O', 'O3', 'CH4', 'NH3']
    rayleigh_species = ['N2', 'O2', 'CO2']
    ret = rt.radtrans(line_species=line_species, rayleigh_species=rayleigh_species)
    ret.setup_opa_structure(press)


    data = []
    p0 = p_trop
    dash = 'solid'
    dgamma = new_d_gamma
    minus = False
    outname = args.outputfile

    alpha = 1
    linewidth = 0.7
    smooth_px = 3
    for i, planet in enumerate(names):
        if i == new_setup:
            # these are changes applied after 3rd planet
            p0 = args.new_p_trop  # New troposphere lower limit
            dash = 'dot'  # New linestyle
            minus = args.plot_difference  # Plot difference instead of spectrum
            dgamma = args.new_d_gamma  # Difference in adiabatic gamma factor

            press = np.logspace(-9, np.log10(new_p_surf), 90)
            ret.setup_opa_structure(press)

        if verbose:
            print("Planet {planet} with temperature {temp}K and radius {rad} R⊕ Earth".format(
                planet=planet,
                temp=temp_list[i],
                rad=radius_list[i])
            )

        for setup in setups_list:
            ### Create Planet object
            planet_inst = Planet(t_eff=temp_list[i], mu=0.001 * setup['mmw'][0], gamma=setup['gamma'] + dgamma)
            grav = gravity_list[i]
            r_pl = radius_list[i] * nc.r_earth
            # TODO: change way to calculate p-t
            temp = planet_inst.p_t_profile(press, p0=p0)
            if args.emission:
                ret.calc_flux(temp, setup['abundances'], grav, setup['mmw'])
                y = ret.flux
            else:
                ret.calc_transm(temp, setup['abundances'], grav, setup['mmw'], p0, r_pl)
                y = ret.transm_rad / nc.r_earth

            # plt.plot(nc.c / ret.freq / 1e-4, ret.transm_rad / nc.r_earth, color='blue', alpha=alpha, linewidth=linewidth)
            plot = go.Scatter(
                x=nc.c / ret.freq / 1e-4,
                y=gaussian_filter1d(y, smooth_px),
                name=planet + '-' + setup['name'],
                visible=True,
                line=dict(
                    color=setup['color'],
                    width=linewidth,
                    dash=dash,
                )
            )
            if minus:
                plot['y'] -= data[0 + (i - args.new_setup) * len(setups_list)]['y']
            data.append(plot)

    steps = []
    step = dict(
        method='restyle',
        args=['visible', [True] * len(data)],
        label='all'
    )
    steps.append(step)
    for i, planet in enumerate(names):
        step = dict(
            method='restyle',
            args=['visible', ["legendonly"] * len(data)],
            label=planet
        )
        step['args'][1][i * len(setups_list):i * len(setups_list) + len(setups_list)] = [True] * len(
            setups_list)  # Toggle i'th trace to "visible"

        steps.append(step)

    sliders = [dict(
        active=0,
        currentvalue={"prefix": "Planet: "},
        # pad = {"t": 50},
        steps=steps
    )]

    layout = go.Layout(
        title="p_surf = {0},{2} bar, p_trop = {1},{3}, d_gamma = {4}".format(
            p_surf, p_trop, new_p_surf, new_p_trop, new_d_gamma
        ),
        xaxis=dict(
            type='log',
            autorange=False,
            title="Wavelength, microns",
            showline=True,
            mirror=True,
            range=[-0.4, 1.5]
        ),
        yaxis=dict(
            autorange=True,
            showline=True,
            mirror=True,
        ),
        sliders=sliders
    )

    fig = go.Figure(data=data, layout=layout)
    py.iplot(fig, filename=outname)
    py.image.save_as(fig, filename=outname + ".png")
    off_iplot(fig, filename=outname)
